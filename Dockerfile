FROM node:16-alpine
RUN apk add --no-cache libc6-compat
RUN mkdir -p /usr/app
WORKDIR  /usr/app

copy ./ ./

RUN npm install
RUN npm run build

EXPOSE 3000
CMD ["npm","start"]

